(add-to-load-path (getcwd))

(use-modules (gnu)
	     (ynm-system-declaration))

(list
 (machine
  (operating-system %ynm-system-declaration)
  (environment digital-ocean-environment-type)
  (configuration (digital-ocean-configuration
                  (region "fra1")
                  (size "s-1vcpu-1gb")
		  (ssh-key "/home/jeko/.ssh/id_rsa")
		  (tags (list "guix" "ynm"))
                  (enable-ipv6? #f)))))
