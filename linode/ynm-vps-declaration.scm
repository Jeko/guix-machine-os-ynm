(add-to-load-path (getcwd))

(use-modules (gnu)
	     (ynm-system-declaration))

(list
 (machine
  (operating-system %ynm-system-declaration)
  (environment managed-host-environment-type)
  (configuration (machine-ssh-configuration
                  (host-name "172.104.243.13")
		  (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIE04Yt1eMCUrfwG4LXAVJs3BSk5F7OSXdVqD5d/waeZ0")
		  (system "x86_64-linux")
                  (user "root")
                  (identity "/home/jeko/.ssh/id_ed25519_linode")
                  (port 2222)))))

